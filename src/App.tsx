import React from 'react';
import Wizard from './components/Wizard/Wizard';
import wizardData from './wizard-obj-samlpe.json';

const App = () => {
  return (
    <div>
      <Wizard wizardData={wizardData} />
    </div>
  );
};

export default App;
