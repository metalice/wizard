import React from 'react';
import { WizardFieldProps } from '../../../../types/wizard';
import WizardNextButton from './WizardNextButton/WizardNextButton';

export const WizardFieldNumber = ({ element, setWizardLocation }: WizardFieldProps) => {
  const { name, question, isLastStep, index } = element;

  return (
    <>
      <div className="Wizard-field-title">{name}</div>
      <div>
        <div className="Wizard-field-question">{question}</div>
        <input type="number" />
      </div>
      <WizardNextButton isLastStep={isLastStep as boolean} index={index as number} setWizardLocation={setWizardLocation} />
    </>
  );
};
