import React from 'react';
import { WizardFieldProps } from '../../../../types/wizard';
import WizardNextButton from './WizardNextButton/WizardNextButton';

const WizardFieldMulti = ({ element, setWizardLocation }: WizardFieldProps) => {
  const { name, question, isLastStep, index, values } = element;

  const field = values?.map((value) => {
    return (
      <>
        <input type="radio" id={value} name={name} value={value} />
        <label htmlFor={value}>{value}</label>
      </>
    );
  });

  return (
    <>
      <div className="Wizard-field-title">{name}</div>
      <div className="Wizard-field-question">{question}</div>
      <div>{field}</div>
      <WizardNextButton isLastStep={isLastStep as boolean} index={index as number} setWizardLocation={setWizardLocation} />
    </>
  );
};

export default WizardFieldMulti;
