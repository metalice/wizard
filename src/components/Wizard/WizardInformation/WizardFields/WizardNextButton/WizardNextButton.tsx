import React from 'react';
import './wizard-next-button.scss';

interface WizardNextButtonProps {
  setWizardLocation: (index: number, isLastStep: boolean) => void;
  isLastStep: boolean;
  index: number;
}

const WizardNextButton: React.FC<WizardNextButtonProps> = ({ setWizardLocation, index, isLastStep }) => {
  return (
    <button className="Wizard-button" onClick={() => setWizardLocation(index, isLastStep)}>
      {isLastStep ? "done" : "next"}
    </button>
  );
};

export default WizardNextButton;
