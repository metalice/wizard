import React, { ReactElement, useMemo } from 'react';
import { WizardSections, WizardStep } from '../../../types/wizard';
import { WizardFieldInput } from './WizardFields/WizardFieldInput';
import WizardFieldMulti from './WizardFields/WizardFieldMulti';
import { WizardFieldNumber } from './WizardFields/WizardFieldNumber';
import './wizard-information.scss';

interface WizardInformationProps {
  data: WizardSections[];
  wizardLocation: number;
  setWizardLocation: (index: number, isLastStep: boolean) => void;
}

const WizardInformation: React.FC<WizardInformationProps> = ({ data, wizardLocation, setWizardLocation }) => {
  const flatSteps = useMemo(() => data?.reduce((acc, section) => [...acc, ...section?.steps], [] as Array<WizardStep>), [data]);

  const field = useMemo(() => {
    const element = flatSteps?.find(({ index }) => index === wizardLocation) as WizardStep;
    const componentMapper: { [key: string]: ReactElement | null } = {
      string: <WizardFieldInput element={element} setWizardLocation={setWizardLocation} />,
      number: <WizardFieldNumber element={element} setWizardLocation={setWizardLocation} />,
      multi: <WizardFieldMulti element={element} setWizardLocation={setWizardLocation} />,
      default: null,
    };

    return componentMapper?.[element?.type || "default"];
  }, [wizardLocation, flatSteps, setWizardLocation]);

  return <div>{field}</div>;
};

export default WizardInformation;
