import React, { useCallback, useMemo, useState } from 'react';
import { WizardData } from '../../types/wizard';
import { wizardDataWithIndexes } from '../../utils/wizard';
import WizardInformation from './WizardInformation/WizardInformation';
import WizardNavigation from './WizardNavigation/WizardNavigation';
import './wizard.scss';

interface WizardProps {
  wizardData: WizardData;
}

const Wizard: React.FC<WizardProps> = ({ wizardData }) => {
  const { name: wizardName } = wizardData;

  const wizardDataExtended = useMemo(() => wizardDataWithIndexes(wizardData), [wizardData]);

  const [wizardLocation, setWizardLocation] = useState<number>(1);

  const setWizardLocationExtended = useCallback(
    (index: number, isLast?: boolean) => {
      setWizardLocation(() => (isLast ? index : index + 1));
    },
    [setWizardLocation]
  );

  return (
    <div className="Wizard-main">
      {wizardName}
      <div className="Wizard-grid">
        <div>
          <WizardNavigation data={wizardDataExtended?.data} wizardLocation={wizardLocation} setWizardLocation={setWizardLocation} />
        </div>
        <div>
          <WizardInformation data={wizardDataExtended?.data} wizardLocation={wizardLocation} setWizardLocation={setWizardLocationExtended} />
        </div>
      </div>
    </div>
  );
};

export default Wizard;
