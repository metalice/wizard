import React from 'react';
import { WizardStep } from '../../../../../types/wizard';

interface WizardNavigationStepProps {
  step: WizardStep;
  setWizardLocation: (index: number) => void;
}

const WizardNavigationStep: React.FC<WizardNavigationStepProps> = ({ step, setWizardLocation }) => {
  const { name, index } = step;

  return <div onClick={() => setWizardLocation(index as number)}>{name}</div>;
};

export default WizardNavigationStep;
