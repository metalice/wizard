import React, { useMemo } from 'react';
import { WizardSections, WizardStep } from '../../../../types/wizard';
import WizardNavigationStep from './WizardNavigationStep/WizardNavigationStep';
import './wizard-navigation-section.scss';

interface WizardNavigationSectionProps {
  section: WizardSections;
  setWizardLocation: (index: number) => void;
}
const WizardNavigationSection: React.FC<WizardNavigationSectionProps> = ({ section, setWizardLocation }) => {
  const { steps, name, index } = section;
  const sectionSteps = useMemo(
    () => steps?.map((step: WizardStep) => <WizardNavigationStep key={step?.name} step={step} setWizardLocation={setWizardLocation} />),
    [steps, setWizardLocation]
  );

  return (
    <div className="Wizard-section">
      <div onClick={() => setWizardLocation(index as number)} className="Wizard-section-title">{name}</div>
      <div className="Wizard-steps-title">{sectionSteps}</div>
    </div>
  );
};

export default WizardNavigationSection;
