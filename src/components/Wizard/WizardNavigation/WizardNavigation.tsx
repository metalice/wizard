import React, { useMemo } from 'react';
import { WizardSections } from '../../../types/wizard';
import WizardNavigationSection from './WizardNavigationSection/WizardNavigationSection';
import './wizard-navigation.scss';

interface WizardNavigationProps {
  data: WizardSections[];
  wizardLocation: number;
  setWizardLocation: (index: number, isLastStep?: boolean) => void;
}

const WizardNavigation: React.FC<WizardNavigationProps> = ({ data, setWizardLocation }) => {
  const wizardSections = useMemo(
    () => data?.map((section: WizardSections) => <WizardNavigationSection key={section?.name} section={section} setWizardLocation={setWizardLocation} />),
    [data, setWizardLocation]
  );

  return <div className="Wizard-navigation">{wizardSections}</div>;
};

export default WizardNavigation;
