interface WizardCommonFields {
  name: string;
}

export interface WizardData extends WizardCommonFields {
  sections: WizardSections[];
}

export interface WizardSections extends WizardCommonFields {
  steps: WizardStep[];
  index?: number;
  isLastSection?: boolean;
}

export interface WizardStep extends WizardCommonFields {
  question: string;
  type: string;
  index?: number;
  values?: string[];
  isLastStep?: boolean;
}

export interface WizardFieldProps {
  element: WizardStep;
  setWizardLocation: (index: number, isLastStep: boolean) => void;
}
