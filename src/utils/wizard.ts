import { WizardData, WizardSections, WizardStep } from '../types/wizard';

export const wizardDataWithIndexes = (wizardData: WizardData) => {
  let index = 0;
  const { sections } = wizardData;
  const data: WizardSections[] = sections?.map((section, sectionIdx) => {
    return {
      ...section,
      index: index + 1,
      steps: section?.steps?.map((step) => {
        const stepWithIndex = { ...step, index: index + 1 };
        index++;
        return stepWithIndex;
      }),
      isLastSection: sectionIdx === sections.length - 1,
    };
  });
  const lastSection = data.pop() as WizardSections;
  const lastStep = { ...lastSection?.steps?.pop(), isLastStep: true } as WizardStep;
  data.push({ ...lastSection, steps: [...lastSection?.steps, lastStep] });
  return { data, index };
};
